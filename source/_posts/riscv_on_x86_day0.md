---
title: riscv on x86 day0
date: 2022-09-25T13:19:59-04:00
tags:
 - linux
 - hexo
 - nodejs
categories:
 - riscv
---
# Riscv playground on x86

参考 https://embeddedinn.xyz/articles/tutorial/Linux-Python-on-RISCV-using-QEMU-from-scratch/

# Prerequisite
## 环境
### Distro
```
✗ lsb_release -a 
No LSB modules are available.
Distributor ID: Debian
Description:    Debian GNU/Linux 11 (bullseye)
Release:        11
Codename:       bullseye
```
Debian官方提供hybrid-iso，可以直接dd到u盘启动。然而在我的老PC（i7-6700）上，重启后u盘识别会失败，同时直接进入安装找不到安装媒介（找不到u盘自己mount的分区？）。重启PC后从u盘引导至debian live系统，终于可以找到/dev/sdb安装介质，安装到格式化后的ssd（/dev/sda）上。

### apt source list
更换清华tuna源。（did I add the last 3 buster sourcelist for apt-file installation？）
```
~ cat /etc/apt/sources.list
# 默认注释了源码镜像以提高 apt update 速度，如有需要可自行取消注释
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye main contrib non-free
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-updates main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-updates main contrib non-free

deb https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-backports main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian/ bullseye-backports main contrib non-free

deb https://mirrors.tuna.tsinghua.edu.cn/debian-security bullseye-security main contrib non-free
# deb-src https://mirrors.tuna.tsinghua.edu.cn/debian-security bullseye-security main contrib non-free

deb http://deb.debian.org/debian buster main contrib non-free
deb http://deb.debian.org/debian-security/ buster/updates main contrib non-free
deb http://deb.debian.org/debian buster-updates main contrib non-free
```

### 更新kernel
kernel.org上下载了linux-6.0-rc6的kernel，解压后进入源文件夹
```
~ sudo apt install build-essential libncurses-dev bison flex libssl-dev libelf-dev
~ cp -v /boot/config-5.10.0-18-amd64 .config
~ make menuconfig
~ make -j6
"" 如果编译时报错，搜索修改.config文件解决
~ vim .config
~ make -j6
~ sudo make modules_install
~ sudo make install
"" 如果有缺少lib警告，可以适当安装对应firmware
~ sudo apt install firmware-linux
```
确认`make install`无报错后重启
```
✗ uname -a      
Linux dan 6.0.0-rc6 #1 SMP PREEMPT_DYNAMIC Thu Sep 22 01:46:24 EDT 2022 x86_64 GNU/Linux
```

### zsh个人配置
~/.zshrc
```
# User configuration

# Get rid of zsh odd completion behaviour in the middle of the filename
# See: https://unix.stackexchange.com/questions/259287/how-can-i-get-zshs-completion-working-in-the-middle-of-the-filename
bindkey '\CI' expand-or-complete-prefix
PATH=/home/dan/node_modules/.bin:/opt/riscv/bin:/opt/riscv/riscv64-unknown-elf/bin:/opt/riscv/riscv64-unknown-linux-gnu/bin:/home/dan/riscv/bin:$PATH
export PATH
```

### hexo on gitlab pull
1. 复制旧PC上已经注册到gitlab的`.ssh key`，`id_ed25519*` 权限为600。
```
✗ ls -la ~/.ssh/
总用量 20
drwxr-xr-x  2 dan dan 4096 Sep 25 12:56 .
drwxr-xr-x 33 dan dan 4096 Sep 25 13:50 ..
-rw-------  1 dan dan  411 Sep 25 12:55 id_ed25519
-rw-------  1 dan dan  101 Sep 25 12:55 id_ed25519.pub
-rw-r--r--  1 dan dan  444 Sep 25 12:56 known_hosts
```
2. 测试ssh key是否通过
```
✗ ssh -T git@gitlab.com
Welcome to GitLab, @isekaijoucyo!
```

3. 拉取并配置hexo
```
✗ git clone git@gitlab.com:isekaijoucyo/isekaijoucyo.gitlab.io.git
✗ npm install hexo-cli
✗ cd isekaijoucyo.gitlab.io
✗ npm install
✗ npm bin
/home/dan/isekaijoucyo.gitlab.io/node_modules/.bin
"" 检查bin运行dir，加入path中
✗ hexo g
"" hexo生成应该可以通过
```

4. 安装vscode以及插件

参考：https://code.visualstudio.com/docs/setup/linux
可以直接通过`sudo apt install ./<file>.deb`安装。
扩展中安装Markdown All in One以及Hexo Utils，即可加载isekaijoucyo.gitlab.io文件夹编辑hexo文章并预览（ctrl+shift+p找一下命令即可）