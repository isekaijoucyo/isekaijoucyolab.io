---
title: riscv_on_x86_day1
date: 2022-09-27T06:08:06-04:00
tags:
 - linux
categories:
 - riscv
---
# Riscv playground on x86

参考 https://embeddedinn.xyz/articles/tutorial/Linux-Python-on-RISCV-using-QEMU-from-scratch/

根据老哥指引单独编译各工具，首先是riscv的gnu toolchain

## GCC toolchain
```
mkdir ~/workbench && cd ~/workbench
git clone https://github.com/riscv/riscv-gnu-toolchain
cd riscv-gnu-toolchain

"" Put riscv stuffs under /opt/riscv/
sudo mkdir /opt/riscv

"" enabled --enable-multilib to build the toolchain with 32-bit and 64-bit support.
./configure --enable-multilib --prefix=/opt/riscv

"" Notice we are compiling the gcc toolchain with the default gcc(x86) toolchain provided by debian 11

"" We build the embedded version only for now
make -j $(nproc)
sudo make install
```
编译生成内容如下（这个时间点前编译的，后来又编译了linux）
```
find /opt/riscv/bin/ -type f ! -newermt '2022-09-21 07:00'
```
{% asset_img "2022-09-27T064805.png" "2022-09-27T064805" %}

## RISCV ISA SIM
- named as **spike**, a RISC-V ISA Simulator
```
cd ~/workbench
git clone https://github.com/riscv-software-src/riscv-isa-sim.git
cd riscv-isa-sim
mkdir build && cd build
../configure --prefix=/opt/riscv
make -j $(nproc)

sudo make install
```

编译生成内容如下（这个时间点之间编译的)
```
find /opt/riscv/bin/ -type f -newermt '2022-09-21 07:00' ! -newermt '2022-09-21 11:57'
```
{% asset_img "2022-09-27T065511.png" "2022-09-27T065511" %}

## riscv-pk 
- **pk**, a.k.a. **proxy kernel**, provides the bootloader and excuting environment for riscv excutables. (This is a riscv executable! )
```
cd ~/workbench
git clone https://github.com/riscv-software-src/riscv-pk.git
mkdir build
cd build

"" Need to take the gcc-toolchain binaries in PATH for the cross compile. 
"" or put export PATH=/opt/riscv/bin:$PATH in ~/.zshrc (I am using zsh)
export PATH=/opt/riscv/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games

```

../configure --prefix=/opt/riscv --host=riscv64-unknown-elf
make -j $(nproc)


编译生成内容如下（这个时间点之间编译的)
```
find /opt/riscv/ -type f -newermt '2022-09-21 11:57' ! -newermt '2022-09-21 12:30'
```
{% asset_img "2022-09-27T070935.png" "2022-09-27T070935" %}
```
file /opt/riscv/riscv64-unknown-elf/bin/pk
/opt/riscv/riscv64-unknown-elf/bin/pk: ELF 64-bit LSB executable, UCB RISC-V, version 1 (SYSV), statically linked, not stripped
```

## 运行riscv可执行文件
流程spike模拟riscv的pk内核，在pk内核上运行riscv可执行文件（static elf only?)
```
➜ mkdir ~/workbench/test && cd $_

"" Edit main.c (hello world program code)
➜ vim main.c

➜ cat main.c
#include <stdio.h>

int main(){
        printf("hello world");
        return 0;
}
➜ /opt/riscv/bin/riscv64-unknown-elf-gcc -Wall main.c -o hello
➜ file hello  
hello: ELF 64-bit LSB executable, UCB RISC-V, version 1 (SYSV), statically linked, with debug_info, not stripped

"" Execute! (Make sure spike/pk are under PATH!)
➜ export PATH=/opt/riscv/bin:/opt/riscv/riscv64-unknown-elf/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
➜ spike pk hello 
bbl loader
hello world%
```
