---
title: Hello Hexo asset_img
author: cj
date: 2022-09-23 13:40:08
tags: gossip
categories: test
---

## Twilight Struggle
### 冷战热斗

{% asset_img "twilight_s" "twilight_s.webp" 600 400 "冷战热斗" "alt twi_s" %}

{% asset_img "twilight_amazon_panel.jpg" "冷战热斗面板" %}

<img src="{% asset_path twilight_amazon.jpg %}" width="600px" title="冷战热斗 600p">

Check out:
```
https://boardgamegeek.com/boardgame/12333/twilight-struggle
```
